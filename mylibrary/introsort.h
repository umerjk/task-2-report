#ifndef INTROSORT
#define INTROSORT

#include<iterator>
#include<algorithm>
#include<functional>
#include<cmath>
#include<vector>
#include"shellsort.h"
#include"selection_sort.h"



namespace mylib
{

    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>

    void introsort_loop( RandomAcessIterator first, RandomAcessIterator last, double limit_or_floor, Compare cmp = Compare())
    {

        //size_type n = last-first;
        //limit_or_floor = 2 * std::floor(std::log2(n));
        int size_threshold = 10;
        while (last-first>size_threshold)
        {
            if(limit_or_floor == 0)
            {
                selection_sort(first,last);
                return;
            }
            else
            {
                --limit_or_floor;
                RandomAcessIterator cut = (first + (last-first)/2);
                //size_type part = std::partition(first, last);
                introsort_loop( cut, last, limit_or_floor);
                last = cut;
            }
        }
    }

    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>



    void itrosort(RandomAcessIterator first, RandomAcessIterator last, Compare cmp = Compare())
    {
        using size_type = typename RandomAcessIterator::difference_type;
        size_type depth_limit_or_floor = 2 * std::floor(std::log2(last-first));

        introsort_loop(first,last,depth_limit_or_floor,cmp);
        shellSort(first,last);
    }
}


#endif // INTROSORT

