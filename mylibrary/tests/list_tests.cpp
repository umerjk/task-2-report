
#include <gtest/gtest.h>

#include "../list.h"



TEST(Container_List,Size_functionality) {


  using namespace mylib;


  List list { 1,2,3,4 };


  EXPECT_EQ(4,list.size());

}
